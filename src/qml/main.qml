/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import org.kde.kirigami 2.11 as Kirigami

Kirigami.ApplicationWindow {

    id: pageRoot
    width: 720
    height: 480
    title: "Atelier Printer Host"
    
    Kirigami.PagePool {
        id: mainPagePool
    }

    globalDrawer: Kirigami.GlobalDrawer{
        modal: false
        width: pageRoot.width / 12
        actions: [
            Kirigami.PagePoolAction {
                iconName: "preferences-system"
                page: Qt.resolvedUrl("profile.qml")
                pagePool: mainPagePool
                tooltip: i18n("Profiles")
            },
            Kirigami.PagePoolAction {
               iconName: "network-connect"
               page: Qt.resolvedUrl("connect.qml")
               pagePool: mainPagePool
               tooltip: i18n("Connect")
            },
            Kirigami.PagePoolAction {
               iconName: "applications-engineering"
               page: Qt.resolvedUrl("axis.qml")
               pagePool: mainPagePool
               tooltip: i18n("Axis and Filament")
            }
        //    Kirigami.PagePoolAction {
        //        iconName: "camera-video"
        //        page: Qt.resolvedUrl("")
        //        pagePool: mainPagePool
        //        tooltip: i18n("Live Video")
        //    },
        //    Kirigami.PagePoolAction {
        //        iconName: "accessories-text-editor"
        //        page: Qt.resolvedUrl("")
        //        pagePool: mainPagePool
        //        tooltip: i18n("Log")
        //    },
        //    Kirigami.PagePoolAction {
        //        iconName: "weather-clear-night"
        //        page: Qt.resolvedUrl("")
        //        pagePool: mainPagePool
        //        tooltip: i18n("Temperatures")
        //    }
        ]
    }
    pageStack.initialPage: mainPagePool.loadPage(Qt.resolvedUrl("profile.qml"))
}
