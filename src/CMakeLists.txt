set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTORCC ON)
set(atelier_SRCS
    main.cpp
)

qt5_add_resources(atelierqml_SRCS resources.qrc)
add_executable(atelier ${atelier_SRCS} ${atelierqml_SRCS})
install(TARGETS atelier RUNTIME DESTINATION bin)

target_link_libraries(atelier
    AtCore::AtCore
    KF5::I18n
    Qt5::Qml
    Qt5::Quick
    Qt5::Widgets
)
